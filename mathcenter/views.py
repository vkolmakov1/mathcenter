from django.shortcuts import render
from django.views.generic import View

from mathcenter.apps.educational_content.models import HelpfulLink


class Home(View):
    def get(self, request, *args, **kwargs):
        helpful_links = HelpfulLink.objects.all()[:5]
        context = {
            "helpful_links": helpful_links
        }
        template_name = "home.html"
        return render(request, template_name, context)


class About(View):
    def get(self, request, *args, **kwargs):
        context = {}
        template_name = "about.html"
        # TODO: Add in an about text
        return render(request, template_name, context)
