from django.db import models
from django.utils import timezone
import requests
import json, datetime

WEEKDAYS = [
    (1, ("Monday")),
    (2, ("Tuesday")),
    (3, ("Wednesday")),
    (4, ("Thursday")),
    (5, ("Friday")),
    (6, ("Saturday")),
    (7, ("Sunday")),
]


def getHours():
    hours = []
    for hour in range(7, 24):
        display_hour = str(hour % 12) if hour != 12 else str(12)
        display_minute = '00'
        postfix = 'am' if hour // 12 == 0 else 'pm'
        hours.append(tuple([hour, display_hour + ":" +
                            display_minute + postfix]))
    return hours

HOURS = getHours()


class DayOff(models.Model):
    class Meta:
        verbose_name_plural = "Days off"
        ordering = ["date"]

    date = models.DateField(blank=False, primary_key=True)

    def get_tutors_off(self, location):
        return Tutor.objects.filter(days_off=self, location=location)

    def __unicode__(self):
        return str(self.date)


class Professor(models.Model):
    name = models.CharField(max_length=120, blank=False, primary_key=True)

    class Meta:
        ordering = ["name"]

    def __unicode__(self):
        return self.name


class Location(models.Model):
    name = models.CharField(max_length=120, blank=False, primary_key=True)

    def __unicode__(self):
        return self.name

class Course(models.Model):
    code = models.CharField(max_length=120, blank=False, primary_key=True)
    name = models.CharField(max_length=120, blank=False)
    color_id = models.IntegerField(default=0)
    professors = models.ManyToManyField(Professor, blank=True)
    location = models.ManyToManyField(Location, blank=True)

    class Meta:
        ordering = ["code"]

    def __unicode__(self):
        return "{}: {}".format(self.code, self.name)


class Tutor(models.Model):
    display_name = models.CharField(max_length=120,
                                    blank=False,
                                    primary_key=True)
    courses = models.ManyToManyField(Course, blank=True)
    days_off = models.ManyToManyField(DayOff, blank=True)
    location = models.ManyToManyField(Location, blank=True)

    class Meta:
        ordering = ["display_name"]

    def get_courses(self):
        return '; '.join([c.code for c in self.courses.all()])

    def __unicode__(self):
        return self.display_name


class Schedule(models.Model):
    weekday = models.IntegerField(choices=WEEKDAYS)
    start_hour = models.IntegerField(choices=HOURS)
    tutors = models.ManyToManyField(Tutor, blank=True)
    location = models.ManyToManyField(Location, blank=True)

    def get_tutors(self):
        return '; '.join([tutor.display_name for tutor in self.tutors.all()])

    def get_weekday(self):
        return WEEKDAYS[self.weekday-1][1]

    def get_hour(self):
        return str(dict(HOURS).get(self.start_hour))

    def get_location(self):
        return str(self.location.get())

    def __unicode__(self):
        return self.get_weekday() + " " + self.get_hour()


class ManageExtensionSettings(models.Model):
    class Meta:
        verbose_name_plural = "Manage settings for chrome extension"
        ordering = ['updated']

    name = models.CharField(max_length=120)
    settings_link = models.CharField(max_length=120)
    updated = models.DateTimeField(auto_now_add=True)
    location = models.ManyToManyField(Location, blank=True)

    def post_settings(self):
        headers = {"content-type": "application/json"}
        url = str(self.settings_link)
        data = ManageSchedulerUtils.generate_settings_dictionary()
        try:
            r = requests.put(url, data=json.dumps(data), headers=headers)
        except Exception:
            return False
        self.updated = timezone.now()
        return True

    def __unicode__(self):
        return str(self.settings_link)


class ManageSchedulerUtils():
    @staticmethod
    def get_tutors_requested_off(location):
        """Returns a dictionary with date strings as keys
        and list of unavailable tutors as values """
        start_date = datetime.datetime.now()
        end_date = start_date + datetime.timedelta(days=128)

        requested_off_days = {}
        try:
            # Grabbing possible days off, that are between start and enddate
            days_off = DayOff.objects.filter(
                date__range=[start_date.strftime("%Y-%m-%d"),
                             end_date.strftime("%Y-%m-%d")])
            # Going over every day off and grabbing tutors that have requested those days off
            for day_off in days_off:
                display_date = day_off.date.strftime("%m-%d-%Y")
                requested_off_days[display_date] = [str(tutor) for tutor in day_off.get_tutors_off(location)]
        except Exception as e:
            print str(e)
            return {}

        return requested_off_days

    @staticmethod
    def get_courses(location):
        courses = {}
        for course_obj in Course.objects.filter(location=location):
            course_name = str(course_obj)
            courses[course_name] = {
                "code": str(course_obj.code),
                "color": str(course_obj.color_id),
                "name": course_name,
            }
        return courses

    @staticmethod
    def get_tutors(location):
        tutors = {}
        for tutor_obj in Tutor.objects.filter(location=location):
            tutor_name = str(tutor_obj.display_name)
            tutors[tutor_name] = []
            for course_obj in tutor_obj.courses.filter(location=location):
                tutors[tutor_name].append(str(course_obj.code))
            tutors[tutor_name].append(str('0'))
        return tutors

    @staticmethod
    def get_professors(location):
        professors = {}
        for course_obj in Course.objects.filter(location=location):
            course_code = str(course_obj.code)
            course_professors = []
            for professor_obj in course_obj.professors.all():
                course_professors.append(str(professor_obj.name))
            course_professors.append("Other")
            professors[course_code] = course_professors
        return professors

    @staticmethod
    def get_schedule(location):
        schedule = {}
        for weekday_value, weekday_name in WEEKDAYS:
            schedule[weekday_name] = {}
            day_obj = Schedule.objects.filter(weekday=weekday_value, location=location).order_by('start_hour')
            for hour_obj in day_obj:
                hour_value = hour_obj.start_hour
                schedule[weekday_name][str(hour_value)] = []
                for tutor_obj in hour_obj.tutors.filter(location=location):
                    schedule[weekday_name][str(hour_value)].append(tutor_obj.display_name)
        return schedule

    @staticmethod
    def generate_settings_dictionary(**kwargs):
        if 'location' not in kwargs:
            location = Location.objects.all()[0]
        else:
            location = kwargs['location']
        return {
            "schedule": ManageSchedulerUtils.get_schedule(location),
            "tutors": ManageSchedulerUtils.get_tutors(location),
            "courses": ManageSchedulerUtils.get_courses(location),
            "professors": ManageSchedulerUtils.get_professors(location),
            "daysOff": ManageSchedulerUtils.get_tutors_requested_off(location),
        }
