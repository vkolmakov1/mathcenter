from django.shortcuts import render
from django.http import JsonResponse
from django.views.generic import View

from mathcenter.apps.manage_scheduler.models import ManageSchedulerUtils as utils

class GetExtensionSettings(View):
    # API call
    def get(self, request, *args, **kwargs):
        location = request.GET.get("location")

        if not location:
            return JsonResponse({
                "error": "Please specify the location"
            })

        settings = utils.generate_settings_dictionary(location=location)
        return JsonResponse(settings)
