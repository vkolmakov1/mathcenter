from django.core.management import BaseCommand


from mathcenter.apps.manage_scheduler.models import *
from mathcenter.apps.scheduler.settings import settings


class Command(BaseCommand):
    help = "Fill up database"

    WEEKDAYS = {
        "Monday": 1,
        "Tuesday": 2,
        "Wednesday": 3,
        "Thursday": 4,
        "Friday": 5,
        "Saturday": 6,
        "Sunday": 7,
    }

    def fill_up_courses(self):
        courses = settings.get("courses")
        for course, data in courses.items():
            name = data.get("name").split(": ")[1]
            code = data.get("code")
            color_id = data.get("color")
            course = Course(name=name, code=code, color_id=color_id)
            course.save()

        return True

    def fill_up_profs(self):
        profs = settings.get("professors")
        prof_set = set()
        for course_code, prof_list in profs.items():
            prof_list.remove("Other")
            for prof in prof_list:
                prof_set.add(prof)

        for prof in prof_set:
            p = Professor(name=prof)
            p.save()

        for course_code, prof_list in profs.items():
            try:
                course = Course.objects.get(code=course_code)
                for prof in prof_list:
                    p = Professor.objects.get(name=prof)
                    course.professors.add(p)
            except Exception:
                print "prof_filling problem"

    def fill_up_tutors(self):
        tutors = settings.get("tutors")
        for tutor_name, course_codes in tutors.items():
            course_codes.remove('0')
            tutor = Tutor.objects.create(display_name=tutor_name)
            for course_code in course_codes:
                try:
                    course = Course.objects.get(code=course_code)
                    tutor.courses.add(course)
                except Exception:
                    print "no such course", course_code

    def fill_up_schedule(self):
        schedule = settings.get("schedule")
        for weekday, hours in schedule.items():
            for hour, tutors in hours.items():
                schedule = Schedule.objects.create(weekday=self.WEEKDAYS.get(weekday), start_hour=int(hour))
                for tutor_name in tutors:
                    try:
                        tutor = Tutor.objects.get(display_name=tutor_name)
                        schedule.tutors.add(tutor)
                    except Exception:
                        print tutor_name, "is not defined"

    def handle(self, *args, **kwargs):
        # self.stdout.write("Filling up courses")
        # self.fill_up_courses()

        # self.stdout.write("Filling up profs")
        # self.fill_up_profs()

        self.stdout.write("Filling up tutors")
        self.fill_up_tutors()

        self.stdout.write("Filling up schedule")
        self.fill_up_schedule()
