from django.core.management import BaseCommand


from mathcenter.apps.manage_scheduler.models import *
from mathcenter.apps.scheduler.writing_center_settings import schedule, tutors


class Command(BaseCommand):
    help = "Populate writing center"

    WEEKDAYS = {
        "Monday": 1,
        "Tuesday": 2,
        "Wednesday": 3,
        "Thursday": 4,
        "Friday": 5,
        "Saturday": 6,
        "Sunday": 7,
    }

    def fill_up_tutors(self):
        loc = Location.objects.get(name="Writing Center")
        for tutor in tutors:
            try:
                t = Tutor.objects.create(display_name=tutor)
                t.location.add(loc)
                t.save()
            except Exception as e:
                print e


    def fill_up_schedule(self):
        loc = Location.objects.get(name="Writing Center")
        for weekday, hours in schedule.items():
            for hour, tutors in hours.items():
                s = Schedule.objects.create(weekday=self.WEEKDAYS.get(weekday), start_hour=int(hour))
                for tutor_name in tutors:
                    try:
                        t = Tutor.objects.get(display_name=tutor_name)
                        s.tutors.add(t)
                        s.location.add(loc)
                    except Exception:
                        print ":("


    def handle(self, *args, **kwargs):
        self.stdout.write("Filling up tutors")
        self.fill_up_tutors()

        self.stdout.write("Filling up schedule")
        self.fill_up_schedule()
