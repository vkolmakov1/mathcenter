# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('manage_scheduler', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='color_id',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='course',
            name='professors',
            field=models.ManyToManyField(to='manage_scheduler.Professor', blank=True),
        ),
    ]
