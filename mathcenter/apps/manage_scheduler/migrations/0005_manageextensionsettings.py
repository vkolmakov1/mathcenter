# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('manage_scheduler', '0004_auto_20151226_1645'),
    ]

    operations = [
        migrations.CreateModel(
            name='ManageExtensionSettings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('settings_link', models.CharField(max_length=120)),
            ],
        ),
    ]
