# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('manage_scheduler', '0009_auto_20160205_1331'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='location',
            field=models.ManyToManyField(to='manage_scheduler.Location', blank=True),
        ),
    ]
