# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('manage_scheduler', '0002_auto_20151226_1421'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tutor',
            name='courses',
            field=models.ManyToManyField(to='manage_scheduler.Course', blank=True),
        ),
    ]
