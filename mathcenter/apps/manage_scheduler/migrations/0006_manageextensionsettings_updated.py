# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('manage_scheduler', '0005_manageextensionsettings'),
    ]

    operations = [
        migrations.AddField(
            model_name='manageextensionsettings',
            name='updated',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 22, 4, 28, 6, 457181, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
    ]
