# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('manage_scheduler', '0003_auto_20151226_1523'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='course',
            options={'ordering': ['code']},
        ),
        migrations.AlterModelOptions(
            name='professor',
            options={'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='tutor',
            options={'ordering': ['display_name']},
        ),
        migrations.AlterField(
            model_name='schedule',
            name='tutors',
            field=models.ManyToManyField(to='manage_scheduler.Tutor', blank=True),
        ),
    ]
