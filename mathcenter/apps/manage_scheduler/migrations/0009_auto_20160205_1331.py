# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('manage_scheduler', '0008_auto_20160123_1453'),
    ]

    operations = [
        migrations.CreateModel(
            name='Location',
            fields=[
                ('name', models.CharField(max_length=120, serialize=False, primary_key=True)),
            ],
        ),
        migrations.AddField(
            model_name='manageextensionsettings',
            name='location',
            field=models.ManyToManyField(to='manage_scheduler.Location', blank=True),
        ),
        migrations.AddField(
            model_name='schedule',
            name='location',
            field=models.ManyToManyField(to='manage_scheduler.Location', blank=True),
        ),
        migrations.AddField(
            model_name='tutor',
            name='location',
            field=models.ManyToManyField(to='manage_scheduler.Location', blank=True),
        ),
    ]
