# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('manage_scheduler', '0006_manageextensionsettings_updated'),
    ]

    operations = [
        migrations.AddField(
            model_name='manageextensionsettings',
            name='name',
            field=models.CharField(default='name', max_length=120),
            preserve_default=False,
        ),
    ]
