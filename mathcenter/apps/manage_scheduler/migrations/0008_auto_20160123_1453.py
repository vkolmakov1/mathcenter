# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('manage_scheduler', '0007_manageextensionsettings_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='DayOff',
            fields=[
                ('date', models.DateField(serialize=False, primary_key=True)),
            ],
            options={
                'ordering': ['date'],
                'verbose_name_plural': 'Days off',
            },
        ),
        migrations.AlterModelOptions(
            name='manageextensionsettings',
            options={'ordering': ['updated'], 'verbose_name_plural': 'Manage settings for chrome extension'},
        ),
        migrations.AddField(
            model_name='tutor',
            name='days_off',
            field=models.ManyToManyField(to='manage_scheduler.DayOff', blank=True),
        ),
    ]
