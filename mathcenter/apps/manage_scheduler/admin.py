from django.contrib import admin

from .models import *


class DayOffAdmin(admin.ModelAdmin):
    class Meta:
        model = DayOff


class LocationAdmin(admin.ModelAdmin):
    class Meta:
        model = Location


class ProfessorAdmin(admin.ModelAdmin):
    class Meta:
        model = Professor


class CourseAdmin(admin.ModelAdmin):
    class Meta:
        model = Course

    filter_horizontal = ('professors', 'location')


class TutorAdmin(admin.ModelAdmin):
    class Meta:
        model = Tutor

    filter_horizontal = ('courses', 'days_off', 'location',)
    list_display = ('display_name', 'get_courses', )


class ScheduleAdmin(admin.ModelAdmin):
    class Meta:
        model = Schedule

    list_filter = ('weekday', 'location',)
    list_display = ('get_hour', 'get_tutors', 'get_location',)
    ordering = ('start_hour',)
    filter_horizontal = ('tutors', 'location',)


class ManageExtensionSettingsAdmin(admin.ModelAdmin):
    class Meta:
        model = ManageExtensionSettings

    def post_schedule(self, request, queryset):
        url_obj = queryset.first()
        if(url_obj.post_settings()):
            self.message_user(request, "Settings have been successfully posted")
        else:
            self.message_user(request, "An error has ocurred, try again later")

    list_display = ('name', 'settings_link', 'updated')
    actions = ['post_schedule']

admin.site.register(Professor, ProfessorAdmin)
admin.site.register(Course, CourseAdmin)
admin.site.register(Tutor, TutorAdmin)
admin.site.register(Schedule, ScheduleAdmin)
admin.site.register(ManageExtensionSettings, ManageExtensionSettingsAdmin)
admin.site.register(DayOff, DayOffAdmin)
admin.site.register(Location, LocationAdmin)
