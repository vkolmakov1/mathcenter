from django.db import models
from mathcenter.apps.manage_scheduler.models import Course


class HelpfulLink(models.Model):
    title = models.CharField(max_length=120, blank=False)
    description = models.CharField(max_length=240, blank=False)
    link = models.CharField(max_length=120, blank=False)
    related_courses = models.ManyToManyField(Course, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["-created"]

    def __unicode__(self):
        return self.title
