# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('manage_scheduler', '0004_auto_20151226_1645'),
    ]

    operations = [
        migrations.CreateModel(
            name='HelpfulLink',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=120)),
                ('description', models.CharField(max_length=240)),
                ('link', models.CharField(max_length=120)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('related_courses', models.ManyToManyField(to='manage_scheduler.Course', blank=True)),
            ],
            options={
                'ordering': ['-created'],
            },
        ),
    ]
