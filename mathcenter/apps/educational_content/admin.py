from django.contrib import admin

from .models import *


class HelpfulLinkAdmin(admin.ModelAdmin):
    class Meta:
        model = HelpfulLink

    filter_horizontal = ('related_courses',)

admin.site.register(HelpfulLink, HelpfulLinkAdmin)
