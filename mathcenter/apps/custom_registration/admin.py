from django.contrib import admin

from .models import MathCenterUser
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User


class MathCenterUserInline(admin.StackedInline):
    model = MathCenterUser
    can_delete = False
    verbose_name_plural = 'Math Center Users'


class UserAdmin(BaseUserAdmin):
    inlines = (MathCenterUserInline, )

admin.site.unregister(User)
admin.site.register(User, UserAdmin)
