from django.db import models

from django.contrib.auth.models import User
from mathcenter.apps.manage_scheduler.models import Course


class MathCenterUser(models.Model):
    user = models.OneToOneField(User, null=False)
    first_name = models.CharField(max_length=30, null=True)
    last_name = models.CharField(max_length=30, null=True)
    math_course = models.ForeignKey(Course, null=True)
    next_appointment_date = models.DateTimeField(null=True, blank=True)
    appointment_calendar_id = models.CharField(max_length=250, null=True, blank=True)
