from django.shortcuts import render
from django.views.generic import View

from django.contrib.auth.models import User
from .models import MathCenterUser

from registration.backends.default.views import RegistrationView
from registration.forms import RegistrationFormUniqueEmail
from mathcenter.apps.custom_registration.forms import MathCenterUserForm


class RegistrationViewUniqueEmail(RegistrationView):
    form_class = RegistrationFormUniqueEmail


class EditMathCenterUserProfile(View):
    form_class = MathCenterUserForm

    def get(self, request, *args, **kwargs):
        user = User.objects.get(username=request.user)
        first_time = False
        try:
            math_center_user = user.mathcenteruser
            message = None
        except Exception:
            math_center_user = MathCenterUser(user=user)
            message = "Before we start scheduling appointments we will need some information about you"
            first_time = True

        first_name = user.mathcenteruser.first_name
        last_name = user.mathcenteruser.last_name
        math_course = user.mathcenteruser.math_course
        next_appointment_date = user.mathcenteruser.next_appointment_date

        data = {
            "first_name": first_name,
            "last_name": last_name,
            "math_course": math_course,
        }

        form = self.form_class(initial=data)

        context = {
            "form": form,
            "message": message,
            "next_appointment_date": next_appointment_date,
            "first_time": first_time,
        }
        template_name = "edit_profile.html"
        return render(request, template_name, context)

    def post(self, request, *args, **kwargs):
        template_name = "edit_profile.html"
        form = self.form_class(request.POST)
        if form.is_valid():
            user = User.objects.get(username=request.user)
            try:
                math_center_user = user.mathcenteruser
            except Exception:
                math_center_user = MathCenterUser(user=user)

            math_center_user.first_name = form.cleaned_data['first_name']
            math_center_user.last_name = form.cleaned_data['last_name']
            math_center_user.math_course = form.cleaned_data['math_course']
            next_appointment_date = user.mathcenteruser.next_appointment_date
            math_center_user.save()

            message = "Changes have been saved"
            context = {
                "form": form,
                "message": message,
                "next_appointment_date": next_appointment_date,
            }

            return render(request, template_name, context)
        else:
            return render(request, template_name, {"form": form})
