from registration.forms import RegistrationFormUniqueEmail
from django import forms

from .models import MathCenterUser
from registration.users import UserModel

User = UserModel()


class RegistrationFormOverride(RegistrationFormUniqueEmail):
    # Kind of a hack but oh well...
    username = forms.CharField(max_length=254, required=False, widget=forms.HiddenInput())

    def clean_password1(self):
        if len(self.cleaned_data['password1']) < 6:
            raise forms.ValidationError(("Make sure your password is at least 6 sybmols long!"))
        return self.cleaned_data['password1']

    def clean_email(self):
        ccc_domains = ['ccc.edu', 'student.ccc.edu']
        email = self.cleaned_data['email']
        self.cleaned_data['username'] = email
        toks = email.split('@')
        if toks[1] not in ccc_domains:
            raise forms.ValidationError(("Please use your .student.ccc.edu or .ccc.edu email address!"))
        if User.objects.filter(email__iexact=self.cleaned_data['email']):
            raise forms.ValidationError(("This email address is already in use. Please supply a different email address."))
        return email


class MathCenterUserForm(forms.ModelForm):
    def clean_first_name(self):
        first_name = self.cleaned_data['first_name']
        toks = []
        for token in first_name.split(' '):
            toks.append(token.capitalize())
        first_name_clean = ' '.join(toks)
        return first_name_clean

    def clean_last_name(self):
        last_name = self.cleaned_data['last_name']
        toks = []
        for token in last_name.split(' '):
            toks.append(token.capitalize())
        last_name_clean = ' '.join(toks)
        return last_name_clean

    class Meta:
        model = MathCenterUser
        fields = ('first_name', 'last_name', 'math_course', )
