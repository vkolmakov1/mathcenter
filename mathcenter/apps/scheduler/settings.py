settings = {
"schedule" :{
    "Friday": {
        "10": [
            "Mike", 
            "Sam", 
            "Eduardo", 
            "0"
        ], 
        "11": [
            "Mike", 
            "Sam", 
            "Eduardo", 
            "0"
        ], 
        "12": [
            "Mike", 
            "Sam", 
            "Eduardo", 
            "0"
        ], 
        "13": [
            "Mike", 
            "Sam", 
            "Eduardo", 
            "0"
        ], 
        "14": [
            "Mike", 
            "Sam", 
            "Eduardo", 
            "0"
        ]
    }, 
    "Monday": {
        "9": [
            "Mike", 
            "Anh", 
            "Sam", 
            "Javier", 
            "0"
        ], 
        "10": [
            "Mike", 
            "Anh", 
            "Sam", 
            "Javier", 
            "0"
        ], 
        "11": [
            "Mike", 
            "Anh", 
            "Sam", 
            "Javier", 
            "0"
        ], 
        "12": [
            "Mike", 
            "Anh", 
            "Sam", 
            "0"
        ], 
        "13": [
            "Mike", 
            "Anh", 
            "Sam", 
            "0"
        ], 
        "14": [
            "Mike", 
            "Kenneth", 
            "Sam", 
            "Paul", 
            "Cesar", 
            "0"
        ], 
        "15": [
            "ChrisT", 
            "Kenneth", 
            "Jeel", 
            "Paul", 
            "Jeenal", 
            "0"
        ], 
        "16": [
            "ChrisT", 
            "Kenneth", 
            "Jeel", 
            "Paul", 
            "Jeenal", 
            "0"
        ], 
        "17": [
            "ChrisT", 
            "Kenneth", 
            "Jeel", 
            "Paul", 
            "Jeenal", 
            "0"
        ], 
        "18": [
            "ChrisT", 
            "Eduardo", 
            "Paul", 
            "0"
        ], 
        "19": [
            "ChrisT", 
            "Eduardo", 
            "Paul", 
            "0"
        ]
    }, 
    "Saturday": {
        "10": [
            "ChrisT", 
            "Vlad", 
            "Eduardo", 
            "0"
        ], 
        "11": [
            "ChrisT", 
            "Vlad", 
            "Eduardo", 
            "0"
        ], 
        "12": [
            "ChrisT", 
            "Vlad", 
            "Eduardo", 
            "0"
        ], 
        "13": [
            "ChrisT", 
            "Vlad", 
            "Eduardo", 
            "0"
        ], 
        "14": [
            "ChrisT", 
            "Vlad", 
            "Eduardo", 
            "0"
        ]
    }, 
    "Thursday": {
        "9": [
            "Mike", 
            "Mae", 
            "Maha", 
            "Sam", 
            "0"
        ], 
        "10": [
            "Mike", 
            "Mae", 
            "Maha", 
            "Sam", 
            "David", 
            "0"
        ], 
        "11": [
            "Mike", 
            "Mae", 
            "Maha", 
            "Sam", 
            "David", 
            "0"
        ], 
        "12": [
            "Mike", 
            "Mae", 
            "ChrisR", 
            "Sam", 
            "Jeenal", 
            "0"
        ], 
        "13": [
            "Mike", 
            "Mae", 
            "ChrisR", 
            "Sam", 
            "Jeenal", 
            "0"
        ], 
        "14": [
            "ChrisT", 
            "ChrisR", 
            "Sam", 
            "Jeenal", 
            "0"
        ], 
        "15": [
            "ChrisT", 
            "Yagmur", 
            "ChrisR", 
            "Vlad", 
            "Jeenal", 
            "0"
        ], 
        "16": [
            "ChrisT", 
            "Yagmur", 
            "ChrisR", 
            "Vlad", 
            "Jeenal", 
            "0"
        ], 
        "17": [
            "ChrisT", 
            "Yagmur", 
            "ChrisR", 
            "Vlad", 
            "Hammad", 
            "0"
        ], 
        "18": [
            "ChrisT", 
            "Yagmur", 
            "ChrisR", 
            "Vlad", 
            "Hammad", 
            "0"
        ], 
        "19": [
            "ChrisT", 
            "Yagmur", 
            "ChrisR", 
            "Vlad", 
            "0"
        ]
    }, 
    "Tuesday": {
        "9": [
            "Mike", 
            "Mae", 
            "Maha", 
            "Sam", 
            "0"
        ], 
        "10": [
            "Mike", 
            "Mae", 
            "Maha", 
            "Sam", 
            "0"
        ], 
        "11": [
            "Mike", 
            "Mae", 
            "Maha", 
            "Sam", 
            "David", 
            "0"
        ], 
        "12": [
            "Mike", 
            "Mae", 
            "Jeenal", 
            "Sam", 
            "David", 
            "0"
        ], 
        "13": [
            "Mike", 
            "Jeenal", 
            "Sam", 
            "David", 
            "0"
        ], 
        "14": [
            "ChrisT", 
            "Jeenal", 
            "Sam", 
            "David", 
            "0"
        ], 
        "15": [
            "ChrisT", 
            "Jeenal", 
            "Yagmur", 
            "David", 
            "0"
        ], 
        "16": [
            "ChrisT", 
            "Jeenal", 
            "Yagmur", 
            "David", 
            "0"
        ], 
        "17": [
            "ChrisT", 
            "Andrew", 
            "Hammad", 
            "Yagmur", 
            "0"
        ], 
        "18": [
            "ChrisT", 
            "Andrew", 
            "Hammad", 
            "Yagmur", 
            "0"
        ], 
        "19": [
            "ChrisT", 
            "Andrew", 
            "Hammad", 
            "Yagmur", 
            "0"
        ]
    }, 
    "Wednesday": {
        "9": [
            "Mike", 
            "Anh", 
            "Sam", 
            "Javier", 
            "0"
        ], 
        "10": [
            "Mike", 
            "Anh", 
            "Sam", 
            "Javier", 
            "0"
        ], 
        "11": [
            "Mike", 
            "Anh", 
            "Sam", 
            "Javier", 
            "0"
        ], 
        "12": [
            "Mike", 
            "Anh", 
            "Sam", 
            "0"
        ], 
        "13": [
            "Mike", 
            "Anh", 
            "Sam", 
            "0"
        ], 
        "14": [
            "Mike", 
            "Kenneth", 
            "Sam", 
            "Paul", 
            "Cesar", 
            "0"
        ], 
        "15": [
            "ChrisT", 
            "Kenneth", 
            "Jeel", 
            "Paul", 
            "Jeenal", 
            "0"
        ], 
        "16": [
            "ChrisT", 
            "Kenneth", 
            "Jeel", 
            "Paul", 
            "Jeenal", 
            "0"
        ], 
        "17": [
            "ChrisT", 
            "Kenneth", 
            "Jeel", 
            "Paul", 
            "Jeenal", 
            "0"
        ], 
        "18": [
            "ChrisT", 
            "Eduardo", 
            "Paul", 
            "0"
        ], 
        "19": [
            "ChrisT", 
            "Eduardo", 
            "Paul", 
            "0"
        ]
    }
},
"tutors" :{
    "Andrew": [
        "125", 
        "0"
    ], 
    "Anh": [
        "118", 
        "125", 
        "146", 
        "208", 
        "210", 
        "PHYS235", 
        "0"
    ], 
    "Bohdan": [
        "0"
    ], 
    "Cesar": [
        "0"
    ], 
    "ChrisR": [
        "118", 
        "125", 
        "0"
    ], 
    "ChrisT": [
        "118", 
        "125", 
        "144", 
        "146", 
        "208", 
        "209", 
        "210", 
        "212", 
        "PHYS235", 
        "PHYS236", 
        "PHYS237", 
        "0"
    ], 
    "David": [
        "0"
    ], 
    "Deep": [
        "208", 
        "210", 
        "PHYS235", 
        "PHYS236", 
        "0"
    ], 
    "Eduardo": [
        "PHYS235", 
        "PHYS236", 
        "PHYS237", 
        "0"
    ], 
    "Everyone": [
        "99", 
        "140", 
        "141", 
        "143", 
        "207", 
        "204", 
        "GED", 
        "0"
    ], 
    "Hammad": [
        "0"
    ], 
    "Javier": [
        "0"
    ], 
    "Jeel": [
        "118", 
        "125", 
        "208", 
        "209", 
        "210", 
        "PHYS235", 
        "PHYS236", 
        "0"
    ], 
    "Jeenal": [
        "118", 
        "125", 
        "208", 
        "209", 
        "210", 
        "PHYS235", 
        "PHYS236", 
        "0"
    ], 
    "Kenneth": [
        "0"
    ], 
    "Mae": [
        "118", 
        "208", 
        "209", 
        "210", 
        "PHYS235", 
        "PHYS236", 
        "0"
    ], 
    "Maha": [
        "118", 
        "125", 
        "208", 
        "PHYS235", 
        "PHYS236", 
        "0"
    ], 
    "Mateusz": [
        "208", 
        "210", 
        "PHYS235", 
        "PHYS236", 
        "0"
    ], 
    "Mike": [
        "118", 
        "125", 
        "144", 
        "146", 
        "208", 
        "209", 
        "210", 
        "212", 
        "0"
    ], 
    "Paul": [
        "208", 
        "209", 
        "210", 
        "PHYS235", 
        "PHYS236", 
        "0"
    ], 
    "Robert": [
        "118", 
        "125", 
        "146", 
        "208", 
        "209", 
        "210", 
        "PHYS235", 
        "PHYS236", 
        "0"
    ], 
    "Sam": [
        "208", 
        "PHYS235", 
        "PHYS236", 
        "PHYS237", 
        "0"
    ], 
    "Usman": [
        "208", 
        "210", 
        "PHYS235", 
        "PHYS236", 
        "0"
    ], 
    "VanessaA": [
        "0"
    ], 
    "Victor": [
        "0"
    ], 
    "Vlad": [
        "125", 
        "144", 
        "146", 
        "208", 
        "209", 
        "210", 
        "0"
    ], 
    "Yagmur": [
        "0"
    ]
},
"courses" :{
    "118: General Education": {
        "code": "118", 
        "color": "8", 
        "name": "118: General Education"
    }, 
    "125: Statistics": {
        "code": "125", 
        "color": "9", 
        "name": "125: Statistics"
    }, 
    "140: College Algebra": {
        "code": "140", 
        "color": "0", 
        "name": "140: College Algebra"
    }, 
    "143: Pre-Calculus": {
        "code": "143", 
        "color": "0", 
        "name": "143: Pre-Calculus"
    }, 
    "144: Finite": {
        "code": "144", 
        "color": "4", 
        "name": "144: Finite"
    }, 
    "146: Discrete": {
        "code": "146", 
        "color": "10", 
        "name": "146: Discrete"
    }, 
    "204: Busines Calculus": {
        "code": "204", 
        "color": "0", 
        "name": "204: Busines Calculus"
    }, 
    "207: Calculus I": {
        "code": "207", 
        "color": "0", 
        "name": "207: Calculus I"
    }, 
    "208: Calculus II": {
        "code": "208", 
        "color": "5", 
        "name": "208: Calculus II"
    }, 
    "209: Calculus III": {
        "code": "209", 
        "color": "6", 
        "name": "209: Calculus III"
    }, 
    "210: Differential Equations": {
        "code": "210", 
        "color": "3", 
        "name": "210: Differential Equations"
    }, 
    "212: Linear Algebra": {
        "code": "212", 
        "color": "7", 
        "name": "212: Linear Algebra"
    }, 
    "99: 98/99/FS Math": {
        "code": "99", 
        "color": "0", 
        "name": "99: 98/99/FS Math"
    }, 
    "GED: GED": {
        "code": "GED", 
        "color": "0", 
        "name": "GED: GED"
    }, 
    "PHYS235: Eng. Physics I": {
        "code": "PHYS235", 
        "color": "7", 
        "name": "PHYS235: Eng. Physics I"
    }, 
    "PHYS236: Eng. Physics II": {
        "code": "PHYS236", 
        "color": "7", 
        "name": "PHYS236: Eng. Physics II"
    }, 
    "PHYS237: Eng. Physics III": {
        "code": "PHYS237", 
        "color": "7", 
        "name": "PHYS237: Eng. Physics III"
    }
},
"professors" :{
    "118": [
        "Ahoyang", 
        "Chung", 
        "Dragos", 
        "Gudehithlu", 
        "Hernandez", 
        "Idacochea", 
        "Ju", 
        "Kolas", 
        "Manoyan", 
        "Miceli", 
        "Motorga", 
        "Nadas", 
        "Papademas", 
        "Podtynov", 
        "Toni", 
        "Venkateswar", 
        "Zimina", 
        "Other"
    ], 
    "125": [
        "Ahmed", 
        "Baliga", 
        "Divani", 
        "Dobria", 
        "Dragos", 
        "Duggal", 
        "Hernandez", 
        "Hester", 
        "Jankowski", 
        "Other"
    ], 
    "140": [
        "Other"
    ], 
    "143": [
        "Annoni", 
        "Dobria", 
        "Duggal", 
        "Joseph", 
        "Kennedy", 
        "Miceli", 
        "Ramirez", 
        "Other"
    ], 
    "144": [
        "Givens", 
        "Nadas", 
        "Other"
    ], 
    "146": [
        "Manoyan", 
        "Other"
    ], 
    "204": [
        "Ju", 
        "Polotsky", 
        "Other"
    ], 
    "207": [
        "Dobria", 
        "Gudehithlu", 
        "Miceli", 
        "Murg", 
        "Other"
    ], 
    "208": [
        "Buchcic", 
        "Colman", 
        "Gudehithlu", 
        "Kamber", 
        "Other"
    ], 
    "209": [
        "Buchcic", 
        "Other"
    ], 
    "210": [
        "Annoni", 
        "Buchcic", 
        "Other"
    ], 
    "212": [
        "Buchcic", 
        "Other"
    ], 
    "99": [
        "Other"
    ], 
    "GED": [
        "Other"
    ], 
    "PHYS": [
        "Other"
    ], 
    "PHYS235": [
        "Dey", 
        "Zanabria", 
        "Other"
    ], 
    "PHYS236": [
        "Kruger", 
        "Other"
    ], 
    "PHYS237": [
        "Zanabria", 
        "Other"
    ]
}
}
