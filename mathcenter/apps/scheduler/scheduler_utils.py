from mathcenter.apps.manage_scheduler.models import ManageSchedulerUtils
from mathcenter.google_api_setup import Google_calendar_api
from django.core.cache import cache
from django.utils import timezone

from calendar import day_name
import datetime
import pytz
import random
import re


class scheduler_utils():
    SUMMARY_REGEX = re.compile(r"(\w+)\s.*\(.+\)\s.*", re.IGNORECASE)
    TIME_FORMAT = "%A, %b %d at %I:%M %p"

    @staticmethod
    def get_open_spots(start_date_str, end_date_str, course):
        open_spots = {}
        # Get settings and create a dictionary based on
        # date_range and course only
        settings = cache.get("settings")
        if not settings:
            settings = ManageSchedulerUtils.generate_settings_dictionary()
            cache.set("settings", settings, 60 * 60)  # Setting cache for 1 hour

        current_date = datetime.datetime.strptime(start_date_str, "%m/%d/%Y")
        end_date = datetime.datetime.strptime(end_date_str, "%m/%d/%Y")
        now = datetime.datetime.now()
        days_off = settings.get('daysOff') if 'daysOff' in settings else {}
        print days_off

        if end_date - current_date > datetime.timedelta(days=7):
            return {
                "error": "Make sure that distance between startDate and endDate is less than or equal to a week"
            }

        # Go over every weekday and filter tutor list based on settings alone
        while current_date <= end_date:
            weekday = day_name[current_date.weekday()]
            open_spots[weekday] = {
                "weekday": weekday,
                "date": current_date.strftime("%m/%d/%Y"),
                "display_date": current_date.strftime("%m/%d"),
                "openSpots": {},
            }

            date_str = current_date.strftime("%m-%d-%Y")
            # grabbing tutors requested day off, if any
            off_tutors = days_off.get(date_str) if date_str in days_off else []

            weekday_obj = settings.get("schedule").get(weekday)
            if weekday_obj:
                for hour in weekday_obj:
                    scheduled_tutors = weekday_obj.get(hour)
                    # Trying to remove senitel value from tutorlist
                    try:
                        scheduled_tutors.remove('0')
                    except ValueError:
                        pass

                    # taking off tutors that have requested off
                    available_tutors = [tutor for tutor in scheduled_tutors if tutor not in off_tutors]

                    open_spots[weekday]["openSpots"][hour] = {}
                    open_spots[weekday]["openSpots"][hour]["numberOfSpots"] = len(available_tutors)

                    # Checking if this time has already passed for a current hour
                    open_spots[weekday]["openSpots"][hour]["open"] = current_date + datetime.timedelta(hours=int(hour)) > now

                    # Trimming off tutors that are not able to tutor given class
                    for tutor in scheduled_tutors:
                        if settings.get("tutors").get(tutor) and not scheduler_utils.can_tutor_course(tutor, course, settings):
                            available_tutors.remove(tutor)
                            open_spots[weekday]["openSpots"][hour]["numberOfSpots"] = len(available_tutors)

            current_date += datetime.timedelta(days=1)

        # Filter this dictionary by connecting to google calendar
        # and subtracting open spots that are scheduled
        open_spots = scheduler_utils.filter_open_spots(open_spots,
                                                       start_date_str,
                                                       end_date_str,
                                                       course,
                                                       settings)
        return open_spots

    @staticmethod
    def filter_open_spots(open_spots, start_date_str, end_date_str, course, settings):
        start_date = datetime.datetime.strptime(start_date_str, "%m/%d/%Y")
        end_date = datetime.datetime.strptime(end_date_str, "%m/%d/%Y")


        start_date = pytz.timezone(Google_calendar_api.TIME_ZONE).localize(start_date)
        end_date = pytz.timezone(Google_calendar_api.TIME_ZONE).localize(end_date)

        service = Google_calendar_api.get_service()
        eventsResult = service.events().list(calendarId=Google_calendar_api.CALENDAR_ID,
                                             timeMin=start_date.isoformat(),
                                             timeMax=end_date.isoformat(),
                                             singleEvents=True,
                                             orderBy='startTime').execute()

        events = eventsResult.get('items', [])
        for event in events:
            # Quick hack to separate offset and datetime
            start_iso_str, offset = event['start'].get('dateTime')[0:19], event['start'].get('dateTime')[19:]
            start_datetime = datetime.datetime.strptime(start_iso_str, "%Y-%m-%dT%H:%M:%S")
            weekday = day_name[start_datetime.weekday()]
            hour = str(start_datetime.hour)
            summary = event['summary']
            tutor_name = scheduler_utils.extract_tutor_name(summary)
            if scheduler_utils.can_tutor_course(tutor_name, course, settings):
                try:
                    open_spots[weekday]["openSpots"][hour]["numberOfSpots"] -= 1
                except KeyError:
                    continue

        return open_spots

    @staticmethod
    def can_schedule(user, date_str, hour):
        name = user.mathcenteruser.first_name + ' ' + user.mathcenteruser.last_name
        course = user.mathcenteruser.math_course

        try:
            next_appointment_date =  user.mathcenteruser.next_appointment_date
            display_datetime_next = timezone.localtime(next_appointment_date).strftime(scheduler_utils.TIME_FORMAT)
        except Exception:
            display_datetime_next = "no appointments have been scheduled"

        date = datetime.datetime.strptime(date_str, "%m/%d/%Y")
        date = pytz.timezone(Google_calendar_api.TIME_ZONE).localize(date)
        date += datetime.timedelta(hours=int(hour))

        display_datetime = date.strftime(scheduler_utils.TIME_FORMAT)
        google_calendar_start_datetime = date.isoformat()
        google_calendar_end_datetime = (date + datetime.timedelta(hours=1)).isoformat()

        now = pytz.timezone(Google_calendar_api.TIME_ZONE).localize(datetime.datetime.now())

        can_schedule = False

        # Broke it up for more readability
        if not next_appointment_date:
            can_schedule = True
        elif date > now and next_appointment_date + datetime.timedelta(minutes=50) < now:
            # appointments are 50 minutes long, hardcoded for now
            can_schedule = True

        response = {
            "can_schedule": can_schedule,
            "display_datetime": display_datetime,
            "display_datetime_next": display_datetime_next,
            "google_calendar_start_datetime": google_calendar_start_datetime,
            "google_calendar_end_datetime": google_calendar_end_datetime,
            "name": name,
            "course": course,
            "weekday": day_name[date.weekday()],
            "hour": hour,
        }
        return response

    @staticmethod
    def schedule_appointment(user, args):
        """
        args = {
        weekday: str,
        hour: str,
        google_calendar_start_datetime: str,
        google_calendar_end_datetime: str,
        }
        """
        service = Google_calendar_api.get_service()
        settings = cache.get("settings")
        if not settings:
            settings = ManageSchedulerUtils.generate_settings_dictionary()
            cache.set("settings", settings, 60 * 60 * 24)  # Setting cache for 24 hours

        tutor_list = settings.get("schedule").get(args["weekday"]).get(args["hour"])
        tutor_list = scheduler_utils.filter_tutor_list(tutor_list,
                                                       args["google_calendar_start_datetime"],
                                                       args["google_calendar_end_datetime"],
                                                       user.mathcenteruser.math_course.code,
                                                       settings,
                                                       service)

        # There are no available tutors, get out!
        if not tutor_list:
            return False
        tutor_name = scheduler_utils.select_random_tutor(tutor_list)
        details = {
            "tutor_name": tutor_name,
            "course_code": user.mathcenteruser.math_course.code,
            "course_name": user.mathcenteruser.math_course,
            "course_color_id": str(user.mathcenteruser.math_course.color_id),
            "student_first_name": user.mathcenteruser.first_name,
            "student_full_name": ' '.join([user.mathcenteruser.first_name, user.mathcenteruser.last_name]),
            "contact": user.username,
            "timestamp": datetime.datetime.now().strftime(scheduler_utils.TIME_FORMAT),
            "professor": "Other",
            "initials": "ONLINE_APP",
        }
        print details.get("course_color_id")
        summary = scheduler_utils.get_appointment_summary(details)
        description = scheduler_utils.get_appointment_description(details)
        event = {
            "summary": summary,
            "description": description,
            "start": {
                "dateTime": args["google_calendar_start_datetime"],
                "timeZone": Google_calendar_api.TIME_ZONE,
            },
            "end": {
                "dateTime": args["google_calendar_end_datetime"],
                "timeZone": Google_calendar_api.TIME_ZONE,
            },
            "colorId": details.get("course_color_id"),
        }

        try:
            event = service.events().insert(calendarId=Google_calendar_api.CALENDAR_ID, body=event).execute()
        except Exception:
            return False
        user.mathcenteruser.appointment_calendar_id = event.get("id")
        start_iso_str, offset = args.get("google_calendar_start_datetime")[0:19], event['start'].get('dateTime')[19:]
        user.mathcenteruser.next_appointment_date = datetime.datetime.strptime(start_iso_str, "%Y-%m-%dT%H:%M:%S")
        user.mathcenteruser.save()
        return True

    @staticmethod
    def remove_appointment(user):
        is_removed = False
        service = Google_calendar_api.get_service()
        try:
            appointment_calendar_id = user.mathcenteruser.appointment_calendar_id
            service.events().delete(calendarId=Google_calendar_api.CALENDAR_ID,
                                    eventId=appointment_calendar_id).execute()
            is_removed = True
        except Exception:
            is_removed = False
        user.mathcenteruser.next_appointment_date = None
        user.mathcenteruser.appointment_calendar_id = None
        user.mathcenteruser.save()
        return is_removed

    @staticmethod
    def get_appointment_summary(details):
        return "{tutor_name} ({student_first_name}) {course_code}".format(**details)

    @staticmethod
    def get_appointment_description(details):
        return """
Student: {student_full_name}
Contact: {contact}
Instructor: {professor}
Course: {course_name}
Created on: {timestamp} by {initials}
        """.format(**details)

    @staticmethod
    def filter_tutor_list(tutor_list, google_calendar_start_datetime, google_calendar_end_datetime, course, settings, service):
        try:
            tutor_list.remove('0')  # Removing sentinel value
        except ValueError:
            pass
        eventsResult = service.events().list(calendarId=Google_calendar_api.CALENDAR_ID,
                                             timeMin=google_calendar_start_datetime,
                                             timeMax=google_calendar_end_datetime,
                                             singleEvents=True,
                                             orderBy='startTime').execute()

        events = eventsResult.get('items', [])
        busy_tutor_list = []
        for event in events:
            summary = event.get("summary")
            busy_tutor_name = scheduler_utils.extract_tutor_name(summary)
            if busy_tutor_name:
                busy_tutor_list.append(busy_tutor_name.lower())

        return [tutor for tutor in tutor_list if tutor.lower() not in busy_tutor_list and scheduler_utils.can_tutor_course(tutor, course, settings)]

    @staticmethod
    def select_random_tutor(tutor_list):
        # TODO: Work a little on the algorithm
        return random.choice(tutor_list)

    @staticmethod
    def extract_tutor_name(summary):
        mo = scheduler_utils.SUMMARY_REGEX.search(summary)
        if mo:
            return mo.group(1)
        else:
            return None

    @staticmethod
    def can_tutor_course(tutor_name, course, settings):
        if tutor_name is None:
            # Didnt match regular expression, keep a spot
            return False
        tutor_courses = settings.get("tutors").get(tutor_name)
        if not tutor_courses:
            # Mathed regular expression but might be misspelled, take off a spot
            print tutor_name + " is not defined!"
            return True
        elif course in tutor_courses or course in settings.get("tutors").get("Everyone"):
            # Matched the regex and can tutor the course, take off!
            return True
        else:
            # Matched the regex but cannot tutor the course, ignore
            return False
