from django.http import JsonResponse
from django.shortcuts import render, redirect

from django.views.generic import View

from mathcenter.apps.manage_scheduler.models import Course
from scheduler_utils import scheduler_utils

from django.contrib.auth.models import User


class SchedulerView(View):
    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return redirect("registration_register")

        user = User.objects.get(username=request.user)
        # Checking if user has his info filled out
        try:
            math_center_user = user.mathcenteruser
        except Exception:
            return redirect("edit_profile")

        template_name = "schedule_appointment.html"
        hour = request.POST.get("hour")
        date = request.POST.get("date")
        if not (hour and date):
            return redirect("display_schedule")

        can_schedule_info = scheduler_utils.can_schedule(user, date, hour)
        context = {
            "date": date,
            "hour": hour,
            "can_schedule_info": can_schedule_info,
        }
        return render(request, template_name, context)

    def get(self, request, *args, **kwargs):
        template_name = "display_schedule.html"
        courses = Course.objects.all()
        try:
            user = User.objects.get(username=request.user)
            initial_course = user.mathcenteruser.math_course
        except Exception:
            initial_course = None

        context = {
            "courses": courses,
            "initial_course": initial_course,
        }
        return render(request, template_name, context)


class GetOpenSpots(View):
    # API call
    def get(self, request, *args, **kwargs):
        start_date = request.GET.get("startDate")
        end_date = request.GET.get("endDate")
        course = request.GET.get("course")
        if not (start_date and end_date and course):
            return JsonResponse({"error": "Please specify startDate, endDate in mm/dd/yyyy format and course"})
        # Do stuff with date range here, that is obtain
        # open spot dictionary based on date range
        open_spots = scheduler_utils.get_open_spots(start_date, end_date, course)
        data = {
            "openSpots": open_spots,
        }
        return JsonResponse(data)


class ScheduleAppointment(View):
    # API Call
    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            user = User.objects.get(username=request.user)
            args = {
                "google_calendar_end_datetime": request.POST.get("google_calendar_end_datetime"),
                "google_calendar_start_datetime": request.POST.get("google_calendar_start_datetime"),
                "hour": request.POST.get("hour"),
                "weekday": request.POST.get("weekday"),
            }
            is_scheduled = scheduler_utils.schedule_appointment(user, args)
            return JsonResponse({"success": is_scheduled})


class RemoveAppointment(View):
    # API Call
    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            user = User.objects.get(username=request.user)
            is_removed = scheduler_utils.remove_appointment(user)
            return JsonResponse({"success": is_removed})
