from django.shortcuts import render
from django.views.generic import View
from django.core.mail import EmailMessage
from mathcenter.settings import EMAIL_HOST_USER

from .forms import ContactForm


class ContactView(View):
    form_class = ContactForm
    template_name = "contact.html"

    def get(self, request, *args, **kwargs):
        context = {
            'form': self.form_class,
        }
        template_name = "contact.html"
        return render(request, template_name, context)

    def post(self, request, *args, **kwargs):
        contact_email = request.POST.get("email")
        message = request.POST.get("message")
        form = self.form_class(request.POST)

        email_body = "From: {}\n\nMessage:\n{}".format(contact_email, message)

        context = {
            "form": form
        }

        if form.is_valid():
            email = EmailMessage(
                subject="Contact Form Submission",
                body=email_body,
                from_email=contact_email,
                to=[EMAIL_HOST_USER],
                reply_to=[contact_email]
            )
            email.send()
            context["status_message"] = "Thank you for your input! We will contact you shortly!"

        return render(request, self.template_name, context)
