from django import forms


class ContactForm(forms.Form):
    email = forms.EmailField(required=True)
    message = forms.CharField(max_length=512, required=True, widget=forms.Textarea)
