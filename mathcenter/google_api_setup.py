from settings import BASE_DIR
import os
import datetime

from oauth2client.client import SignedJwtAssertionCredentials
from httplib2 import Http
from apiclient.discovery import build


class Google_calendar_api():
    CLIENT_EMAIL = os.environ["SERVICE_ACCOUNT_EMAIL"]
    TIME_ZONE = "US/Central"
    CURRENT_YEAR = datetime.datetime.now().year
    PRIVATE_KEY_PATH = os.path.join(BASE_DIR, 'ConvertedPrivateKey.pem')
    CALENDAR_ID = os.environ["CALENDAR_ID"]
    @staticmethod
    def get_credentials():
        with open(Google_calendar_api.PRIVATE_KEY_PATH) as f:
            private_key = f.read()
        credentials = SignedJwtAssertionCredentials(Google_calendar_api.CLIENT_EMAIL, private_key, 'https://www.googleapis.com/auth/calendar')
        return credentials

    @staticmethod
    def get_service():
        credentials = Google_calendar_api.get_credentials()
        service = build('calendar', 'v3', http=credentials.authorize(Http()))
        return service
