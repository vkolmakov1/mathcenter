"""mathcenter URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

from django.contrib.auth.decorators import login_required

from mathcenter.apps.scheduler.views import SchedulerView, GetOpenSpots, ScheduleAppointment, RemoveAppointment
from .views import Home, About
from mathcenter.apps.custom_registration.views import EditMathCenterUserProfile
from mathcenter.apps.contact.views import ContactView
from mathcenter.apps.manage_scheduler.views import GetExtensionSettings

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', Home.as_view(), name='home'),
    url(r'^contact/', ContactView.as_view(), name='contact'),
    url(r'^about/', About.as_view(), name='about'),
    url(r'^editProfile/', login_required(EditMathCenterUserProfile.as_view()), name='edit_profile'),
    url(r'^displaySchedule/', SchedulerView.as_view(), name='display_schedule'),
    url(r'api/getExtensionSettings', GetExtensionSettings.as_view(), name='get_settings'),
    url(r'^api/getOpenSpots', GetOpenSpots.as_view(), name='get_open_spots_ajax'),
    url(r'^api/scheduleAppointment', login_required(ScheduleAppointment.as_view()), name='schedule_appointment_ajax'),
    url(r'^api/removeAppointment', login_required(RemoveAppointment.as_view()), name='remove_appointment_ajax'),
    url(r'^accounts/', include('registration.backends.default.urls')),
]
