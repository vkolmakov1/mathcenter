$(document).ready(function(){
    $("#weekly-date-picker").datetimepicker({
    	format: 'MM/DD/YYYY',
        defaultDate: new Date(),
    });

    $('#weekly-date-picker').on('dp.change', function (e) {
        var value = $("#weekly-date-picker").val();
	var startDate = moment(value, "MM/DD/YYYY").day(0).format("MM/DD/YYYY");
	var endDate =  moment(value, "MM/DD/YYYY").day(7).format("MM/DD/YYYY");
	$("#weekly-date-picker").val(startDate);
	$("#weekly-date-picker-addon").val(endDate)
    });
});
