# Math Center Webapp #

This is a self-service student-faced web application for scheduling tutoring appointments, that I've made for a tutoring center, where I currently work.

Main goal of the project was to provide students an ability to schedule a tutoring appointment, or see our availability without having to physically visit our math center, which could potentially save students a lot of time and provide a them a better customer service experience.

# Screenshots #

Main page view

![1](/screenshots/1.png)

Display schedule view: every green button is a potential open spot for an appointment

![2](/screenshots/2.png)

After clicking on an open spot:

![3](/screenshots/3.png)

Resulting appointment in the google calendar:

![4](/screenshots/4.png)

Student profile screen that allows to change course and/or manage scheduled appointment

![5](/screenshots/5.png)